package com.mitrich.piku.views

import android.content.Context
import com.mitrich.piku.R
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.utils.setBronzeColor
import com.mitrich.piku.utils.setGoldColor
import com.mitrich.piku.utils.setSilverColor
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.top_chooser_item.view.*

class TopChooserItem(private val chooser: Chooser, private val chooserIndex: Int, private val context: Context) :
    Item<ViewHolder>() {
    override fun getLayout() = R.layout.top_chooser_item

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.apply {
            top_chooser_name.text = chooser.name
            chooser_number_of_completions.text = chooser.numberOfCompletions.toString()
            when (chooserIndex) {
                0 -> crown_image.setGoldColor(context)
                1 -> crown_image.setSilverColor(context)
                2 -> crown_image.setBronzeColor(context)
            }
        }
    }
}