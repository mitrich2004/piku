package com.mitrich.piku.views

import android.content.Context
import android.view.View
import com.mitrich.piku.R
import com.mitrich.piku.data.Element
import com.mitrich.piku.utils.setBackgroundLayout
import com.mitrich.piku.utils.setBronzeColor
import com.mitrich.piku.utils.setGoldColor
import com.mitrich.piku.utils.setSilverColor
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.element_item.view.*

class ElementItem(private val element: Element, private val elementIndex: Int, val context: Context) :
    Item<ViewHolder>() {
    override fun getLayout() = R.layout.element_item

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.apply {
            element_item_layout.setBackgroundLayout()
            element_name_text.text = element.name
            element_number_of_championships_text.text = element.numberOfChampionships.toString()
            element_number_of_wins_text.text = element.numberOfWins.toString()

            if (elementIndex < 3) {
                crown_image.visibility = View.VISIBLE
            } else {
                crown_image.visibility = View.GONE
            }

            when (elementIndex) {
                0 -> crown_image.setGoldColor(context)
                1 -> crown_image.setSilverColor(context)
                2 -> crown_image.setBronzeColor(context)
            }
        }
    }
}