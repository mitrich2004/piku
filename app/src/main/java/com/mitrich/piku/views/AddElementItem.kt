package com.mitrich.piku.views

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import com.mitrich.piku.R
import com.mitrich.piku.activities.CreateChooserActivity.Companion.allElementsSaved
import com.mitrich.piku.data.Element
import com.mitrich.piku.utils.setAccentColor
import com.mitrich.piku.utils.setBackgroundLayout
import com.mitrich.piku.utils.setNotActiveColor
import com.mitrich.piku.utils.showToast
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.add_element_item.view.*

class AddElementItem(
    private val element: Element,
    private val elementIndex: Int,
    private val imm: InputMethodManager,
    private val elementsAdapter: GroupAdapter<ViewHolder>,
    private val elementsList: ArrayList<Element>,
    private val saveImage: ImageView,
    private val chooserNameEditText: EditText,
    private val context: Context
) : Item<ViewHolder>() {
    override fun getLayout() = R.layout.add_element_item

    override fun bind(viewHolder: ViewHolder, position: Int) {
        var elementName = element.name
        finishEditing(viewHolder)

        viewHolder.itemView.apply {

            add_element_layout.setBackgroundLayout()

            element_name.text = elementName
            
            add_element_layout.setOnClickListener {
                element_name.visibility = View.GONE
                edit_image.visibility = View.GONE
                done_image.visibility = View.VISIBLE
                element_name_edit_text.visibility = View.VISIBLE
                if (elementName == context.getString(R.string.untitled)) {
                    element_name_edit_text.setText("")
                } else {
                    element_name_edit_text.setText(elementName)
                    element_name_edit_text.setSelection(elementName.length)
                }
                element_name_edit_text.requestFocus()
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
                allElementsSaved = false
                saveImage.setNotActiveColor(context)
            }

            element_name_edit_text.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    allElementsSaved = true
                    if (chooserNameEditText.text.toString().isNotBlank()) {
                        saveImage.setAccentColor(context)
                    }
                    finishEditing(viewHolder)
                    element_name.text = elementName
                    try {
                        saveElement(elementName)
                    } catch (error: IndexOutOfBoundsException) {
                        //activity finished
                    }
                }
            }

            done_image.setOnClickListener {
                val newElementName = element_name_edit_text.text.toString()
                if (newElementName.isNotBlank()) {
                    elementName = newElementName
                    if (chooserNameEditText.text.toString().isNotBlank()) {
                        saveImage.setAccentColor(context)
                    }
                    allElementsSaved = true
                    finishEditing(viewHolder)
                    element_name.text = newElementName
                    saveElement(newElementName)
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                } else {
                    showToast(context, context.getString(R.string.input_name_for_element))
                }
            }

            element_name_edit_text.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (element_name_edit_text.text.toString().isNotBlank()) {
                        done_image.setAccentColor(context)
                    } else {
                        done_image.setNotActiveColor(context)
                    }
                }

                override fun afterTextChanged(s: Editable?) {}
            })
        }
    }

    private fun finishEditing(viewHolder: ViewHolder) {
        viewHolder.itemView.apply {
            element_name.visibility = View.VISIBLE
            edit_image.visibility = View.VISIBLE
            done_image.visibility = View.GONE
            element_name_edit_text.visibility = View.GONE
        }
    }

    private fun saveElement(elementName: String) {
        elementsList[elementIndex] = Element(
            elementName,
            0,
            0,
            element.id
        )
        elementsAdapter.removeGroup(elementIndex)
        elementsAdapter.add(
            elementIndex,
            AddElementItem(
                elementsList[elementIndex],
                elementIndex,
                imm,
                elementsAdapter,
                elementsList,
                saveImage,
                chooserNameEditText,
                context
            )
        )
    }
}