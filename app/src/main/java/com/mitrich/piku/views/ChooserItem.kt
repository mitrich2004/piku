package com.mitrich.piku.views

import android.content.Context
import android.view.View
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.currentUser
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.data.User
import com.mitrich.piku.utils.*
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.chooser_item_layout.view.*

class ChooserItem(val chooser: Chooser, val context: Context) : Item<ViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.chooser_item_layout
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.apply {

            chooser_name_text.text = chooser.name
            chooser_item_layout.setBackgroundLayout()
            if (currentUser.uid != chooser.userId) {
                like_image.visibility = View.VISIBLE
                user_image.visibility = View.GONE
                if (currentUser.likedChoosersList.contains(chooser.id)) {
                    like_image.setAccentColor(context)
                    like_image.setImageResource(R.drawable.ic_favorite)
                } else {
                    like_image.setNotActiveColor(context)
                    like_image.setImageResource(R.drawable.ic_not_favorite)
                }
            } else {
                like_image.visibility = View.GONE
                user_image.visibility = View.VISIBLE
            }

            like_image.setOnClickListener {
                val likedChoosersListReference = getLikedChoosersListReference(currentUser.uid)

                val creatorReference = getUserReference(chooser.userId)

                if (currentUser.likedChoosersList.contains(chooser.id)) {
                    chooser.numberOfLikes -= 1
                    like_image.setNotActiveColor(context)
                    like_image.setImageResource(R.drawable.ic_not_favorite)
                    currentUser.likedChoosersList.remove(chooser.id)
                    likedChoosersListReference.setValue(currentUser.likedChoosersList)
                } else {
                    chooser.numberOfLikes += 1
                    like_image.setAccentColor(context)
                    like_image.setImageResource(R.drawable.ic_favorite)
                    currentUser.likedChoosersList.add(chooser.id)
                    likedChoosersListReference.setValue(currentUser.likedChoosersList)
                }

                creatorReference.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val creator = snapshot.getValue(User::class.java)!!
                        val creatorChoosersList = creator.choosersList
                        for (creatorChooser in creatorChoosersList) {
                            if (chooser.id == creatorChooser.id) {
                                val creatorChooserIndex =
                                    creatorChoosersList.indexOf(creatorChooser)
                                if (currentUser.likedChoosersList.contains(chooser.id)) {
                                    val numberOfLikesReference =
                                        getNumberOfLikesReference(creator.uid, creatorChooserIndex)
                                    numberOfLikesReference.setValue(creatorChooser.numberOfLikes + 1)
                                } else {
                                    val numberOfLikesReference =
                                        getNumberOfLikesReference(creator.uid, creatorChooserIndex)
                                    numberOfLikesReference.setValue(creatorChooser.numberOfLikes - 1)
                                }
                            }
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        showToast(context, error.message)
                    }
                })
            }
        }
    }
}