package com.mitrich.piku.data

data class UserSettings (val languageTag: String, val appThemeId: Int, val accentColorId: Int)