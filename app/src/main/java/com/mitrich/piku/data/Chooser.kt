package com.mitrich.piku.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Chooser(
    val id: String,
    var name: String,
    var elementsList: ArrayList<Element>,
    val userId: String,
    var numberOfLikes: Int,
    var numberOfCompletions: Int
) : Parcelable {
    constructor() : this("", "", arrayListOf<Element>(), "", 0, 0)
}