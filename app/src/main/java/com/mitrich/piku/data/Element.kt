package com.mitrich.piku.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Element(var name: String, var numberOfChampionships: Int, var numberOfWins: Int, val id: String): Parcelable {
    constructor(): this("", 0, 0, "")
}