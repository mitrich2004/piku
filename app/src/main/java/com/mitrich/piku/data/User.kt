package com.mitrich.piku.data

import android.os.Parcelable
import com.mitrich.piku.data.Chooser
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    var username: String,
    val uid: String,
    var profileImageUrl: String,
    var choosersList: ArrayList<Chooser>,
    var likedChoosersList: ArrayList<String>) :
    Parcelable {
    constructor() : this("", "", "", arrayListOf<Chooser>(), arrayListOf<String>())
}


