package com.mitrich.piku.utils

import com.google.firebase.database.*

val mDatabase = FirebaseDatabase.getInstance()
val usersReference = mDatabase.getReference("/users/")

fun getUserReference(userId: String): DatabaseReference {
    return mDatabase.getReference("/users/$userId")
}

fun getUserProfileImageUrlReference(userId: String): DatabaseReference {
    return mDatabase.getReference("/users/$userId/profileImageUrl")
}

fun getNumberOfCompletionsReference(userId: String, chooserIndex: Int): DatabaseReference {
    return mDatabase.getReference("/users/$userId/choosersList/$chooserIndex/numberOfCompletions")
}

fun getNumberOfLikesReference(userId: String, chooserIndex: Int): DatabaseReference {
    return mDatabase.getReference("/users/$userId/choosersList/$chooserIndex/numberOfLikes")
}

fun getNumberOfChampionshipsReference(
    userId: String,
    chooserIndex: Int,
    elementIndex: Int
): DatabaseReference {
    return mDatabase.getReference("/users/$userId/choosersList/$chooserIndex/elementsList/$elementIndex/numberOfChampionships")
}

fun getNumberOfWinsReference(
    userId: String,
    chooserIndex: Int,
    elementIndex: Int
): DatabaseReference {
    return mDatabase.getReference("/users/$userId/choosersList/$chooserIndex/elementsList/$elementIndex/numberOfWins")
}

fun getChoosersListReference(userId: String): DatabaseReference {
    return mDatabase.getReference("/users/$userId/choosersList")
}

fun getLikedChoosersListReference(userId: String): DatabaseReference {
    return mDatabase.getReference("/users/$userId/likedChoosersList")
}

fun getUsernameReference(userId: String): DatabaseReference {
    return mDatabase.getReference("/users/$userId/username")
}