package com.mitrich.piku.utils

import com.mitrich.piku.data.User

fun getNumberOfLikes(user: User): String {
    var numberOfLikes = 0
    for (chooser in user.choosersList) {
        numberOfLikes += chooser.numberOfLikes
    }
    return numberOfLikes.toString()
}

fun getNumberOfCompletions(user: User): String {
    var numberOfCompletions = 0
    for (chooser in user.choosersList) {
        numberOfCompletions += chooser.numberOfCompletions
    }
    return numberOfCompletions.toString()
}

fun getHighestNumberOfLikes(user: User): String {
    var numberOfMostLikes = 0
    for (chooser in user.choosersList) {
        if (chooser.numberOfLikes > numberOfMostLikes) {
            numberOfMostLikes = chooser.numberOfLikes
        }
    }
    return numberOfMostLikes.toString()
}

fun getHighestNumberOfCompletions(user: User): String {
    var numberOfMostCompletions = 0
    for (chooser in user.choosersList) {
        if (chooser.numberOfCompletions > numberOfMostCompletions) {
            numberOfMostCompletions = chooser.numberOfCompletions
        }
    }
    return numberOfMostCompletions.toString()
}