package com.mitrich.piku.utils

import android.content.Context
import android.graphics.PorterDuff
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.carlosmuvi.segmentedprogressbar.SegmentedProgressBar
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.accentColorId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.squareup.picasso.Picasso

val listOfAccentColorsIds = arrayListOf(
    R.color.accent_blue,
    R.color.accent_green,
    R.color.accent_red,
    R.color.accent_purple,
    R.color.accent_orange,
    R.color.accent_pink,
    R.color.accent_sea,
    R.color.accent_navy
)

fun ImageView.setAccentColor(context: Context) {
    setColorFilter(
        ContextCompat.getColor(context, accentColorId),
        PorterDuff.Mode.SRC_IN
    )
}

fun ProgressBar.setAccentColor(context: Context) {
    indeterminateDrawable.setColorFilter(
        ContextCompat.getColor(context, accentColorId),
        PorterDuff.Mode.SRC_IN
    )
}

fun ImageView.setNotActiveColor(context: Context) {
    setColorFilter(
        ContextCompat.getColor(context, R.color.not_active),
        PorterDuff.Mode.SRC_IN
    )
}

fun TextView.setNotActiveColor(context: Context) {
    setTextColor(ContextCompat.getColor(context, R.color.not_active))
}

fun ImageView.setGoldColor(context: Context) {
    setColorFilter(
        ContextCompat.getColor(context, R.color.gold),
        PorterDuff.Mode.SRC_IN
    )
}

fun ImageView.setSilverColor(context: Context) {
    setColorFilter(
        ContextCompat.getColor(context, R.color.silver),
        PorterDuff.Mode.SRC_IN
    )
}

fun ImageView.setBronzeColor(context: Context) {
    setColorFilter(
        ContextCompat.getColor(context, R.color.bronze),
        PorterDuff.Mode.SRC_IN
    )
}

fun TextView.setAccentColor(context: Context) {
    setTextColor(ContextCompat.getColor(context, accentColorId))
}

fun TextView.setElementBackground(colorId: Int) {
    when (colorId) {
        R.color.accent_blue -> setBackgroundResource(R.drawable.element_background_blue)
        R.color.accent_orange -> setBackgroundResource(R.drawable.element_background_orange)
        R.color.accent_green -> setBackgroundResource(R.drawable.element_background_green)
        R.color.accent_purple -> setBackgroundResource(R.drawable.element_background_purple)
        R.color.accent_red -> setBackgroundResource(R.drawable.element_background_red)
        R.color.accent_pink -> setBackgroundResource(R.drawable.element_background_pink)
        R.color.accent_sea -> setBackgroundResource(R.drawable.element_background_sea)
        R.color.accent_navy -> setBackgroundResource(R.drawable.element_background_navy)
    }
}

fun ImageButton.setAccentColor() {
    when (accentColorId) {
        R.color.accent_blue -> setBackgroundResource(R.drawable.round_button_background_blue)
        R.color.accent_orange -> setBackgroundResource(R.drawable.round_button_background_orange)
        R.color.accent_green -> setBackgroundResource(R.drawable.round_button_background_green)
        R.color.accent_purple -> setBackgroundResource(R.drawable.round_button_background_puprple)
        R.color.accent_red -> setBackgroundResource(R.drawable.round_button_background_red)
    }
}

fun SegmentedProgressBar.setAccentColor(context: Context) {
    setFillColor(ContextCompat.getColor(context, accentColorId))
    when (accentColorId) {
        R.color.accent_blue -> setContainerColor(
            ContextCompat.getColor(
                context,
                R.color.accent_blue_transparent
            )
        )
        R.color.accent_orange -> setContainerColor(
            ContextCompat.getColor(
                context,
                R.color.accent_orange_transparent
            )
        )
        R.color.accent_green -> setContainerColor(
            ContextCompat.getColor(
                context,
                R.color.accent_green_transparent
            )
        )
        R.color.accent_purple -> setContainerColor(
            ContextCompat.getColor(
                context,
                R.color.accent_purple_transparent
            )
        )
        R.color.accent_red -> setContainerColor(
            ContextCompat.getColor(
                context,
                R.color.accent_red_transparent
            )
        )
    }
}

fun ImageView.loadUserAvatar(userImageUrl: String) {
    if (userImageUrl != "") {
        Picasso.get().load(userImageUrl).into(this)
    } else {
        Picasso.get().load(R.drawable.user_avatar).into(this)
    }
}

fun View.setBackgroundColor() {
    if (appThemeId == R.style.AppThemeLight) {
        setBackgroundResource(R.color.whitiesh)
    } else {
        setBackgroundResource(R.color.dark_theme_layout)
    }
}

fun SeekBar.setAccentColor(context: Context) {
    this.progressDrawable.setColorFilter(
        ContextCompat.getColor(context, accentColorId),
        PorterDuff.Mode.SRC_IN
    )
    this.thumb.setColorFilter(
        ContextCompat.getColor(context, accentColorId),
        PorterDuff.Mode.SRC_IN
    )
}

fun View.setBackgroundLayout() {
    if (appThemeId == R.style.AppThemeLight) {
        setBackgroundResource(R.drawable.round_corners_layout_light)
    } else {
        setBackgroundResource(R.drawable.round_corners_layout_dark)
    }
}

fun Button.setDisabledButtonLayout() {
    if (appThemeId == R.style.AppThemeDark) {
        setBackgroundResource(R.drawable.not_active_button_background_dark)
    } else {
        setBackgroundResource(R.drawable.not_active_button_background)
    }
}

fun RecyclerView.setBackgroundColor() {
    if (appThemeId == R.style.AppThemeDark) {
        setBackgroundResource(R.drawable.items_recycler_view_background_dark)
    } else {
        setBackgroundResource(R.drawable.items_recycler_view_background)
    }
}

fun EditText.setBackgroundLayout() {
    if (appThemeId == R.style.AppThemeDark) {
        setBackgroundResource(R.drawable.edit_text_background_dark)
    } else {
        setBackgroundResource(R.drawable.edit_text_background)
    }
}

fun showMenuIcons(popupMenu: PopupMenu) {
    val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
    fieldMPopup.isAccessible = true
    val mPopup = fieldMPopup.get(popupMenu)
    mPopup.javaClass
        .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
        .invoke(mPopup, true)

}