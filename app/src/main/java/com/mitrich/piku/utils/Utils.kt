package com.mitrich.piku.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.widget.Toast
import com.mitrich.piku.activities.ChooserActivity
import com.mitrich.piku.views.ChooserItem
import java.util.*

@SuppressLint("SdCardPath")
const val filesDirection = "/data/user/0/com.mitrich.piku/files"
const val userSettingsFileName = "userSettings.json"
const val INTENT_KEY_CHOOSER = "chooser"
const val INTENT_KEY_EDITABLE = "editable"
const val INTENT_KEY_USER = "user"

fun startActivityWithoutAnimation(context: Context, activity: Class<*>) {
    val intent = Intent(context, activity)
    intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
    context.startActivity(intent)
}

fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

fun intentToChooserActivity(context: Context, chooserItem: ChooserItem, editable: Boolean) {
    val intent = Intent(context, ChooserActivity::class.java)
    val chooser = chooserItem.chooser
    intent.putExtra(INTENT_KEY_CHOOSER, chooser)
    intent.putExtra(INTENT_KEY_EDITABLE, editable)
    context.startActivity(intent)
}

fun setLocale(lang: String, baseContext: Context) {
    val locale = Locale(lang)
    Locale.setDefault(locale)
    val config = Configuration()
    config.locale = locale
    baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
}

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "DEPRECATION")
fun isInternetAvailable(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!
        .isConnected
}