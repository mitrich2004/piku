package com.mitrich.piku.activities

import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.accentColorId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.languageTag
import com.mitrich.piku.data.UserSettings
import com.mitrich.piku.utils.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.activity_settings.profile_image
import kotlinx.android.synthetic.main.activity_settings.settings_image
import kotlinx.android.synthetic.main.activity_settings.worldwide_image
import java.io.File

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        settings_bottom_toolbar.setBackgroundColor()
        language_layout.setBackgroundLayout()
        mode_layout.setBackgroundLayout()
        color_layout.setBackgroundLayout()
        settings_image.setAccentColor(this)

        val listOfButtons = listOf(
            english_language_button,
            russian_language_button,
            dark_mode_button,
            light_mode_button,
            blue_color_button,
            green_color_button,
            purple_color_button,
            orange_color_button,
            red_color_button
        )

        listOfButtons.forEach {
            it.buttonTintList =
                (ColorStateList.valueOf(ContextCompat.getColor(this, accentColorId)))
        }

        russian_language_button.isChecked = languageTag == "ru"
        english_language_button.isChecked = languageTag == "en"
        light_mode_button.isChecked = appThemeId == R.style.AppThemeLight
        dark_mode_button.isChecked = appThemeId == R.style.AppThemeDark

        when (accentColorId) {
            R.color.accent_blue -> blue_color_button.isChecked = true
            R.color.accent_green -> green_color_button.isChecked = true
            R.color.accent_purple -> purple_color_button.isChecked = true
            R.color.accent_orange -> orange_color_button.isChecked = true
            R.color.accent_red -> red_color_button.isChecked = true
        }

        english_language_button.setOnClickListener {
            if (languageTag != "en") {
                languageTag = "en"
                setLocale(languageTag, baseContext)
                applyChanges()
            }
        }

        russian_language_button.setOnClickListener {
            if (languageTag != "ru") {
                languageTag = "ru"
                setLocale(languageTag, baseContext)
                applyChanges()
            }
        }

        light_mode_button.setOnClickListener {
            if (appThemeId != R.style.AppThemeLight) {
                appThemeId = R.style.AppThemeLight
                setTheme(appThemeId)
                applyChanges()
            }
        }

        dark_mode_button.setOnClickListener {
            if (appThemeId != R.style.AppThemeDark) {
                appThemeId = R.style.AppThemeDark
                setTheme(appThemeId)
                applyChanges()
            }
        }

        blue_color_button.setOnClickListener {
            if (accentColorId != R.color.accent_blue) {
                accentColorId = R.color.accent_blue
                applyChanges()
            }
        }

        green_color_button.setOnClickListener {
            if (accentColorId != R.color.accent_green) {
                accentColorId = R.color.accent_green
                applyChanges()
            }
        }

        purple_color_button.setOnClickListener {
            if (accentColorId != R.color.accent_purple) {
                accentColorId = R.color.accent_purple
                applyChanges()
            }
        }

        orange_color_button.setOnClickListener {
            if (accentColorId != R.color.accent_orange) {
                accentColorId = R.color.accent_orange
                applyChanges()
            }
        }

        red_color_button.setOnClickListener {
            if (accentColorId != R.color.accent_red) {
                accentColorId = R.color.accent_red
                applyChanges()
            }
        }

        worldwide_image.setOnClickListener {
            startActivityWithoutAnimation(this, AllChoosersActivity::class.java)
        }

        profile_image.setOnClickListener {
            startActivityWithoutAnimation(this, ProfileActivity::class.java)
        }

        settings_local_image.setOnClickListener {
            startActivityWithoutAnimation(this, UserChoosersActivity::class.java)
        }
    }

    private fun applyChanges() {
        File("$filesDir/$userSettingsFileName").writeText(
            Gson().toJson(
                UserSettings(
                    languageTag,
                    appThemeId,
                    accentColorId
                )
            )
        )
        finish()
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivityWithoutAnimation(this, ProfileActivity::class.java)
    }
}