package com.mitrich.piku.activities

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.data.User
import com.mitrich.piku.utils.getUserReference
import com.mitrich.piku.utils.setBackgroundLayout
import com.mitrich.piku.utils.setDisabledButtonLayout
import com.mitrich.piku.utils.showToast
import id.voela.actrans.AcTrans
import kotlinx.android.synthetic.main.activity_entry.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import kotlin.system.exitProcess

class RegisterActivity : AppCompatActivity(), KeyboardVisibilityEventListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry)

        email_edit_text.setBackgroundLayout()
        enter_button.setDisabledButtonLayout()
        password_edit_text.setBackgroundLayout()

        activity_name_text.text = getString(R.string.register_account)
        enter_button.text = getString(R.string.register_text)
        change_way_of_entry_text.text = getString(R.string.already_have_account)

        enter_button.setOnClickListener {
            registerNewUser()
        }

        change_way_of_entry_text.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            AcTrans.Builder(this).performSlideToLeft()
        }

        email_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyRegisterButtonState()
            }
        })

        password_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyRegisterButtonState()
            }
        })
    }

    private fun verifyRegisterButtonState() {
        if (email_edit_text.text.toString().isNotBlank()
            && password_edit_text.text.toString().isNotBlank()
        ) {
            enter_button.setTextColor(ContextCompat.getColor(this, R.color.white))
            enter_button.setBackgroundResource(R.drawable.active_button_background)
        } else {
            enter_button.setTextColor(ContextCompat.getColor(this, R.color.icons_color))
            enter_button.setDisabledButtonLayout()
        }
    }

    private fun registerNewUser() {
        val email = email_edit_text.text.toString()
        val password = password_edit_text.text.toString()

        if (email.isNotBlank() && password.isNotBlank()) {
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnSuccessListener {
                    saveUserToDatabase()
                }
                .addOnFailureListener {
                    showToast(this, it.message!!)
                }
        } else {
            showToast(this, getString(R.string.input_email_and_password))
        }
    }

    private fun saveUserToDatabase() {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val userReference = getUserReference(uid)

        val user = User(getString(R.string.nameless), uid, "", arrayListOf(), arrayListOf())

        userReference.setValue(user)
            .addOnSuccessListener {
                val intent = Intent(this, UserChoosersActivity::class.java)
                startActivity(intent)
            }
            .addOnFailureListener {
                showToast(this, it.message!!)
            }
    }

    override fun onVisibilityChanged(isOpen: Boolean) {
        if (isOpen) {
            entry_activity_scroll_view.scrollTo(0, entry_activity_scroll_view.bottom)
        } else {
            entry_activity_scroll_view.scrollTo(0, entry_activity_scroll_view.top)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }
}