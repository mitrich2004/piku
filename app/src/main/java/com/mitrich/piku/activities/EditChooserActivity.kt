package com.mitrich.piku.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.SeekBar
import com.mitrich.piku.*
import com.mitrich.piku.activities.CreateChooserActivity.Companion.allElementsSaved
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.currentUser
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.data.Element
import com.mitrich.piku.utils.*
import com.mitrich.piku.views.AddElementItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_create_chooser.*
import java.util.*
import kotlin.math.log
import kotlin.math.pow

class EditChooserActivity : AppCompatActivity() {

    val elementsAdapter = GroupAdapter<ViewHolder>()
    var elementsList = arrayListOf<Element>()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_chooser)

        val chooser = intent.getParcelableExtra<Chooser>(INTENT_KEY_CHOOSER)!!
        if (chooser.elementsList.isNotEmpty()) {
            elementsList = chooser.elementsList
        } else {
            elementsList.add(
                Element(
                    getString(R.string.untitled),
                    0,
                    0,
                    UUID.randomUUID().toString()
                ),
            )
            elementsList.add(
                Element(
                    getString(R.string.untitled),
                    0,
                    0,
                    UUID.randomUUID().toString()
                ),
            )
        }

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
        chooser_name_edit_text.requestFocus()

        add_elements_recycler_view.adapter = elementsAdapter

        create_chooser_toolbar_title.text = chooser.name
        create_chooser_toolbar.setBackgroundColor()
        save_image.setAccentColor(this)
        add_elements_recycler_view.setBackgroundColor()
        number_of_elements_seek_bar.setAccentColor(this)
        chooser_name_edit_text.setText(chooser.name)
        chooser_name_edit_text.setSelection(chooser.name.length)
        seek_bar_value_text.text = elementsList.size.toString()
        number_of_elements_seek_bar.progress =
            log(elementsList.size.toDouble(), 2.0).toInt() - 1

        for (i in 0 until elementsList.size) {
            elementsAdapter.add(
                AddElementItem(
                    elementsList[i],
                    i,
                    imm,
                    elementsAdapter,
                    elementsList,
                    save_image,
                    chooser_name_edit_text,
                    this
                )
            )
        }

        create_chooser_back_image.setOnClickListener {
            elementsAdapter.clear()
            elementsList.clear()
            allElementsSaved = true
            finish()
        }

        save_image.setOnClickListener {
            val chooserName = chooser_name_edit_text.text.toString()
            if (chooserName.isNotBlank() && chooserName.isNotEmpty()) {
                if (allElementsSaved) {
                    saveEditedChooser(chooserName, chooser)
                } else {
                    showToast(this, getString(R.string.save_elements))
                }
            } else {
                showToast(this, getString(R.string.name_for_your_chooser))
            }
        }

        val seekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                val numberOfItems = 2.toFloat().pow(seekBar.progress + 1).toInt()
                seek_bar_value_text.text = numberOfItems.toString()

                if (elementsList.size > numberOfItems) {
                    while (elementsList.size > numberOfItems) {
                        elementsList.removeAt(elementsList.size - 1)
                        elementsAdapter.removeGroup(elementsAdapter.groupCount - 1)
                    }
                } else {
                    while (elementsList.size < numberOfItems) {
                        elementsList.add(
                            Element(
                                getString(R.string.untitled),
                                0,
                                0,
                                UUID.randomUUID().toString()
                            )
                        )
                        elementsAdapter.add(
                            AddElementItem(
                                elementsList[elementsList.size - 1],
                                elementsList.size - 1,
                                imm,
                                elementsAdapter,
                                elementsList,
                                save_image,
                                chooser_name_edit_text,
                                this@EditChooserActivity
                            )
                        )
                    }
                }
            }
        }
        number_of_elements_seek_bar.setOnSeekBarChangeListener(seekBarChangeListener)

        chooser_name_edit_text.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    verifySaveImageState()
                }
            })

    }

    private fun verifySaveImageState() {
        if (chooser_name_edit_text.text.toString().isNotBlank() && allElementsSaved) {
            save_image.setAccentColor(this)
        } else {
            save_image.setNotActiveColor(this)
        }
    }

    private fun saveEditedChooser(chooserName: String, chooser: Chooser) {
        var editedChooserIndex = -1
        for (userChooser in currentUser.choosersList) {
            if (userChooser.id == chooser.id) {
                editedChooserIndex = currentUser.choosersList.indexOf(userChooser)
            }
        }
        currentUser.choosersList.removeAt(editedChooserIndex)
        currentUser.choosersList.add(
            editedChooserIndex,
            Chooser(
                chooser.id,
                chooserName,
                elementsList,
                chooser.userId,
                chooser.numberOfLikes,
                chooser.numberOfCompletions
            )
        )
        val choosersListReference = getChoosersListReference(currentUser.uid)
        choosersListReference.setValue(currentUser.choosersList)
        clearElementsList()
        val userChoosersIntent = Intent(this, UserChoosersActivity::class.java)
        startActivity(userChoosersIntent)
    }

    private fun clearElementsList() {
        elementsAdapter.clear()
        elementsList.clear()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        clearElementsList()
    }
}