package com.mitrich.piku.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.views.ElementItem
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.utils.INTENT_KEY_CHOOSER
import com.mitrich.piku.utils.setBackgroundColor
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_elements.*

class ElementsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_elements)

        elements_toolbar.setBackgroundColor()

        val chooser = intent.getParcelableExtra<Chooser>(INTENT_KEY_CHOOSER)!!

        val elementsAdapter = GroupAdapter<ViewHolder>()
        elements_recycler_view.adapter = elementsAdapter
        elements_toolbar_title.text = chooser.name

        val elementsList = chooser.elementsList
        elementsList.sortByDescending { it.numberOfWins }
        elementsList.sortByDescending { it.numberOfChampionships }

        for (element in elementsList) {
            elementsAdapter.add(ElementItem(element, elementsList.indexOf(element), this))
        }

        elements_back_image.setOnClickListener {
            finish()
        }
    }
}