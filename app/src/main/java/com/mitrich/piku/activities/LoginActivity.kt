package com.mitrich.piku.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.utils.setBackgroundLayout
import com.mitrich.piku.utils.setDisabledButtonLayout
import com.mitrich.piku.utils.showToast
import id.voela.actrans.AcTrans
import kotlinx.android.synthetic.main.activity_entry.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import kotlin.system.exitProcess

class LoginActivity : AppCompatActivity(), KeyboardVisibilityEventListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry)

        activity_name_text.text = getString(R.string.log_into_account)
        enter_button.text = getString(R.string.log_in_text)
        change_way_of_entry_text.text = getString(R.string.dont_have_account)

        email_edit_text.setBackgroundLayout()
        enter_button.setDisabledButtonLayout()
        password_edit_text.setBackgroundLayout()

        enter_button.setOnClickListener {
            logInUser()
        }

        change_way_of_entry_text.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            AcTrans.Builder(this).performSlideToRight()
        }

        email_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyLoginButtonState()
            }
        })

        password_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyLoginButtonState()
            }
        })
    }

    private fun verifyLoginButtonState() {
        if (email_edit_text.text.toString().isNotBlank()
            && password_edit_text.text.toString().isNotBlank()
        ) {
            enter_button.setTextColor(ContextCompat.getColor(this, R.color.white))
            enter_button.setBackgroundResource(R.drawable.active_button_background)
        } else {
            enter_button.setTextColor(ContextCompat.getColor(this, R.color.icons_color))
            enter_button.setDisabledButtonLayout()
        }
    }

    private fun logInUser() {
        val email = email_edit_text.text.toString()
        val password = password_edit_text.text.toString()

        if (email.isNotBlank() && password.isNotBlank()) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnSuccessListener {
                    val userChoosersIntent = Intent(this, UserChoosersActivity::class.java)
                    startActivity(userChoosersIntent)
                    finish()
                }
                .addOnFailureListener {
                    showToast(this, it.message!!)
                }
        } else {
            showToast(this, getString(R.string.input_email_and_password))
        }
    }

    override fun onVisibilityChanged(isOpen: Boolean) {
        if (isOpen) {
            entry_activity_scroll_view.scrollTo(0, entry_activity_scroll_view.bottom)
        } else {
            entry_activity_scroll_view.scrollTo(0, entry_activity_scroll_view.top)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }
}