package com.mitrich.piku.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.mitrich.piku.*
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.currentUser
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.data.Element
import com.mitrich.piku.data.User
import com.mitrich.piku.utils.*
import kotlinx.android.synthetic.main.activity_chooser.*
import kotlinx.android.synthetic.main.activity_chooser.number_of_completions_text
import kotlinx.android.synthetic.main.activity_chooser.number_of_likes_text
import kotlinx.android.synthetic.main.activity_create_chooser.*
import kotlinx.android.synthetic.main.activity_elements.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile.user_avatar_image
import kotlinx.android.synthetic.main.activity_profile.username_text
import kotlinx.android.synthetic.main.user_profile_dialog.*

class ChooserActivity : AppCompatActivity() {

    private val winnersList = arrayListOf<Element>()
    private var elementsList = arrayListOf<Element>()
    var chooser = Chooser()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chooser)

        chooser = intent.getParcelableExtra(INTENT_KEY_CHOOSER)!!
        val editable = intent.getBooleanExtra(INTENT_KEY_EDITABLE, false)

        view_elements_button.setOnClickListener {
            val intent = Intent(this, ElementsActivity::class.java)
            intent.putExtra(INTENT_KEY_CHOOSER, chooser)
            startActivity(intent)
        }

        prepareElementsList()

        chooser_toolbar_title.isSelected = true
        chooser_toolbar.setBackgroundColor()
        stage_progress_bar.setAccentColor(this)
        setElementsBackground()

        chooser_toolbar_title.text = chooser.name
        number_of_likes_text.text = chooser.numberOfLikes.toString()
        number_of_completions_text.text = chooser.numberOfCompletions.toString()

        if (chooser.userId != currentUser.uid && !editable) {
            loadUserAvatar()
        }

        if (editable) {
            edit_chooser_image.visibility = View.VISIBLE
            user_avatar_image.visibility = View.GONE
        } else {
            edit_chooser_image.visibility = View.GONE
            user_avatar_image.visibility = View.VISIBLE
        }

        chooser_back_image.setOnClickListener {
            clearAllLists()
            finish()
        }

        edit_chooser_image.setOnClickListener {
            val intent = Intent(this, EditChooserActivity::class.java)
            intent.putExtra(INTENT_KEY_CHOOSER, chooser)
            startActivity(intent)
        }

        user_avatar_image.setOnClickListener {
            val userProfileDialog = Dialog(this)
            userProfileDialog.apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                setCancelable(true)
                setContentView(R.layout.user_profile_dialog)
                window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                val userReference = getUserReference(chooser.userId)
                userReference.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = snapshot.getValue(User::class.java)!!

                        profile_dialog_layout.setBackgroundLayout()
                        user_avatar_image.loadUserAvatar(user.profileImageUrl)
                        username_text.text = user.username
                        number_of_user_choosers_text.text =
                            getString(R.string.number_of_choosers, user.choosersList.size)

                        val numberOfLikes = getNumberOfLikes(user)
                        val numberOfCompletions = getNumberOfCompletions(user)
                        val numberOfMostLikes = getHighestNumberOfLikes(user)
                        val numberOfMostCompletions = getHighestNumberOfCompletions(user)

                        number_of_likes_text.text = numberOfLikes
                        number_of_completions_text.text = numberOfCompletions
                        most_likes_text.text = numberOfMostLikes
                        most_completions_text.text = numberOfMostCompletions

                        view_user_choosers_button.setOnClickListener {
                            val intent = Intent(this@ChooserActivity, ChoosersActivity::class.java)
                            intent.putExtra(INTENT_KEY_USER, user)
                            startActivity(intent)
                            dismiss()
                        }

                        close_image.setOnClickListener {
                            dismiss()
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        showToast(this@ChooserActivity, error.message)
                    }
                })
                show()
            }
        }
    }

    private fun prepareElementsList() {
        if (chooser.elementsList.isNotEmpty()) {
            for (element in chooser.elementsList) {
                elementsList.add(element)
            }
            stage_progress_bar.setSegmentCount(elementsList.size / 2)
            elementsList.shuffle()
            eliminationProcess()
        } else {
            top_element_name_text.visibility = View.GONE
            bottom_element_name_text.visibility = View.GONE
            view_elements_button.visibility = View.GONE
            stage_progress_bar.visibility = View.GONE
            stage_text.visibility = View.GONE
            or_image.visibility = View.GONE
            data_is_damaged_text.visibility = View.VISIBLE
        }
    }

    private fun eliminationProcess() {
        top_element_name_text.text = elementsList[0].name
        bottom_element_name_text.text = elementsList[1].name

        when (elementsList.size + winnersList.size) {
            in 33..65 -> stage_text.text = getString(R.string.stage_1_32)
            in 17..33 -> stage_text.text = getString(R.string.stage_1_16)
            in 9..17 -> stage_text.text = getString(R.string.stage_1_8)
            in 5..9 -> stage_text.text = getString(R.string.stage_1_4)
            in 3..5 -> stage_text.text = getString(R.string.stage_1_2)
            in 1..3 -> stage_text.text = getString(R.string.stage_final)
        }

        top_element_name_text.setOnClickListener {
            vote(0)
        }

        bottom_element_name_text.setOnClickListener {
            vote(1)
        }
    }

    private fun vote(index: Int) {
        stage_progress_bar.incrementCompletedSegments()
        winnersList.add(elementsList[index])
        updateChooserData(elementsList[index], winnersList.size, elementsList.size)
        if (elementsList.size > 2) {
            elementsList = removeElements(elementsList)
            eliminationProcess()
        } else {
            if (winnersList.size != 1) {
                elementsList = removeElements(elementsList)
                winnersList.shuffle()
                for (element in winnersList) {
                    elementsList.add(element)
                }
                winnersList.clear()
                stage_progress_bar.setSegmentCount(elementsList.size / 2)
                stage_progress_bar.reset()
                eliminationProcess()
            } else {
                val winner = winnersList[0]

                top_element_name_text.visibility = View.GONE
                bottom_element_name_text.visibility = View.GONE
                winner_element_name_text.visibility = View.VISIBLE
                winner_element_name_text.text = winner.name
                or_image.visibility = View.GONE
                stage_text.visibility = View.GONE
                stage_progress_bar.visibility = View.GONE
            }
        }
    }

    private fun updateChooserData(winner: Element, winnersListSize: Int, elementsListSize: Int) {
        val userReference = getUserReference(chooser.userId)

        userReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(User::class.java)!!
                val userChoosersList = user.choosersList
                for (completedChooser in userChoosersList) {
                    val chooserIndex =
                        userChoosersList.indexOf(completedChooser)
                    if (chooser.id == completedChooser.id) {
                        for (element in completedChooser.elementsList) {
                            if (element.id == winner.id) {
                                val elementIndex =
                                    completedChooser.elementsList.indexOf(element)
                                if (winnersListSize == 1 && elementsListSize <= 2) {

                                    val numberOfCompletionsReference =
                                        getNumberOfCompletionsReference(
                                            user.uid, chooserIndex
                                        )
                                    val newNumberOfCompletions =
                                        completedChooser.numberOfCompletions + 1
                                    numberOfCompletionsReference.setValue(newNumberOfCompletions)
                                    number_of_completions_text.text =
                                        newNumberOfCompletions.toString()
                                    val numberOfChampionshipsReference =
                                        getNumberOfChampionshipsReference(
                                            user.uid,
                                            chooserIndex,
                                            elementIndex
                                        )
                                    numberOfChampionshipsReference.setValue(element.numberOfChampionships + 1)
                                    element.numberOfChampionships += 1
                                    chooser.elementsList[elementIndex] = element
                                }
                                val numberOfWinsReference =
                                    getNumberOfWinsReference(
                                        user.uid,
                                        chooserIndex,
                                        elementIndex
                                    )
                                numberOfWinsReference.setValue(element.numberOfWins + 1)
                                element.numberOfWins += 1
                                chooser.elementsList[elementIndex] = element
                            }
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@ChooserActivity, error.message)
            }
        })
    }

    private fun loadUserAvatar() {
        val userProfileImageUrlReference = getUserProfileImageUrlReference(chooser.userId)

        userProfileImageUrlReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val userImageUrl = snapshot.getValue(String::class.java) ?: ""
                user_avatar_image.loadUserAvatar(userImageUrl)
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@ChooserActivity, error.toString())
            }
        })
    }

    private fun removeElements(elementsList: ArrayList<Element>): ArrayList<Element> {
        elementsList.removeAt(0)
        elementsList.removeAt(0)
        return elementsList
    }

    private fun setElementsBackground() {
        val listOfColors = arrayListOf<Int>()
        for (color in listOfAccentColorsIds) {
            listOfColors.add(color)
        }
        listOfColors.shuffle()
        top_element_name_text.setElementBackground(listOfColors[0])
        bottom_element_name_text.setElementBackground(listOfColors[1])
    }

    private fun clearAllLists() {
        elementsList.clear()
        winnersList.clear()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        clearAllLists()
    }
}
