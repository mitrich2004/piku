package com.mitrich.piku.activities

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.mitrich.piku.*
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.currentUser
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.data.User
import com.mitrich.piku.utils.*
import com.mitrich.piku.views.ChooserItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import id.voela.actrans.AcTrans
import kotlinx.android.synthetic.main.activity_all_choosers.*
import kotlinx.android.synthetic.main.activity_all_choosers.profile_image
import kotlinx.android.synthetic.main.activity_user_choosers.*
import kotlinx.android.synthetic.main.activity_user_choosers.local_image
import kotlinx.android.synthetic.main.activity_user_choosers.worldwide_image
import java.lang.Exception

class AllChoosersActivity : AppCompatActivity() {

    private val allChoosersAdapter = GroupAdapter<ViewHolder>()
    private val allChoosersList = arrayListOf<Chooser>()
    private val likedChoosersList = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_choosers)

        worldwide_image.setAccentColor(this)
        all_choosers_loading_circle.setAccentColor(this)
        all_choosers_recycler_view.adapter = allChoosersAdapter
        all_choosers_bottom_toolbar.setBackgroundColor()
        all_choosers_toolbar.setBackgroundColor()

        getUserLikedChoosersList()
        fetchAllChoosers()

        local_image.setOnClickListener {
            startActivityWithoutAnimation(this, UserChoosersActivity::class.java)
        }

        profile_image.setOnClickListener {
            startActivityWithoutAnimation(this, ProfileActivity::class.java)
        }

        all_choosers_settings_image.setOnClickListener {
            startActivityWithoutAnimation(this, SettingsActivity::class.java)
        }

        allChoosersAdapter.setOnItemClickListener { item, _ ->
            val chooserItem = item as ChooserItem
            allChoosersAdapter.clear()
            allChoosersList.clear()
            intentToChooserActivity(this, chooserItem, false)
        }

        sort_image.setOnClickListener { view ->
            val popupMenu = PopupMenu(this, view)

            popupMenu.setOnMenuItemClickListener { menuItem ->
                allChoosersAdapter.clear()
                allChoosersList.sortByDescending { likedChoosersList.contains(it.id) }
                when (menuItem.itemId) {
                    R.id.item_most_popular -> {
                        allChoosersList.sortByDescending { it.numberOfCompletions }
                    }

                    R.id.item_most_favored -> {
                        allChoosersList.sortByDescending { it.numberOfLikes }
                    }

                    R.id.item_alphabetic -> {
                        allChoosersList.sortByDescending { it.name }
                    }
                }
                for (chooser in allChoosersList) {
                    allChoosersAdapter.add(ChooserItem(chooser, this@AllChoosersActivity))
                }
                true
            }

            popupMenu.inflate(R.menu.sorting_menu)

            try {
                showMenuIcons(popupMenu)
            } catch (e: Exception) {
                Log.e("ERROR", "Error showing menu icons", e)
            } finally {
                popupMenu.show()
            }
        }

        search_chooser_edit_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            @SuppressLint("DefaultLocale")
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                allChoosersAdapter.clear()
                allChoosersList.forEach {
                    val chooserName = it.name
                    if (chooserName.toLowerCase().contains(s.toString().toLowerCase())) {
                        allChoosersAdapter.add(ChooserItem(it, this@AllChoosersActivity))
                    }
                }
                if (allChoosersAdapter.itemCount == 0) {
                    no_such_choosers_text.visibility = View.VISIBLE
                } else {
                    no_such_choosers_text.visibility = View.GONE
                }
            }
        })
    }

    private fun fetchAllChoosers() {
        usersReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach { dataSnapshot ->
                    val user = dataSnapshot.getValue(User::class.java)!!
                    val userChoosersList = user.choosersList
                    for (chooser in userChoosersList) {
                        allChoosersList.add(chooser)
                    }
                }
                loadChoosers()
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@AllChoosersActivity, error.message)
            }
        })
    }

    private fun loadChoosers() {
        allChoosersList.sortByDescending { likedChoosersList.contains(it.id) }
        for (chooser in allChoosersList) {
            allChoosersAdapter.add(ChooserItem(chooser, this))
        }
        all_choosers_loading_circle.visibility = View.GONE
    }

    private fun getUserLikedChoosersList() {
        val userReference = getUserReference(currentUser.uid)
        userReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(User::class.java)!!
                val userLikedChoosersList = user.likedChoosersList

                for (chooserId in userLikedChoosersList) {
                    likedChoosersList.add(chooserId)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@AllChoosersActivity, error.message)
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        allChoosersAdapter.clear()
        allChoosersList.clear()
        fetchAllChoosers()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, UserChoosersActivity::class.java))
        AcTrans.Builder(this).performSlideToLeft()
    }
}