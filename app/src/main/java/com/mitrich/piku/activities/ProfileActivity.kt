package com.mitrich.piku.activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.media.ExifInterface
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.PopupMenu
import androidx.core.app.ActivityCompat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.currentUser
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.utils.*
import com.mitrich.piku.views.TopChooserItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import id.voela.actrans.AcTrans
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile.settings_image
import kotlinx.android.synthetic.main.activity_user_choosers.*
import kotlinx.android.synthetic.main.activity_user_choosers.local_image
import kotlinx.android.synthetic.main.activity_user_choosers.profile_image
import kotlinx.android.synthetic.main.activity_user_choosers.worldwide_image
import kotlinx.android.synthetic.main.change_username_dialog.*
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.util.*

class ProfileActivity : AppCompatActivity() {
    val topChooserAdapter = GroupAdapter<ViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        profile_image.setAccentColor(this)
        profile_layout.setBackgroundLayout()
        profile_bottom_toolbar.setBackgroundColor()
        top_choosers_layout.setBackgroundLayout()
        top_choosers_loading_circle.setAccentColor(this)

        user_avatar_image.loadUserAvatar(currentUser.profileImageUrl)
        username_text.text = currentUser.username
        number_of_choosers_text.text =
            getString(R.string.number_of_choosers, currentUser.choosersList.size)

        val numberOfLikes = getNumberOfLikes(currentUser)
        val numberOfCompletions = getNumberOfCompletions(currentUser)
        val numberOfMostLikes = getHighestNumberOfLikes(currentUser)
        val numberOfMostCompletions = getHighestNumberOfCompletions(currentUser)

        number_of_likes_text.text = numberOfLikes
        number_of_completions_text.text = numberOfCompletions
        number_of_most_likes_text.text = numberOfMostLikes
        number_of_most_completions_text.text = numberOfMostCompletions
        top_choosers_recyclerview.adapter = topChooserAdapter
        loadTopChoosersList()

        username_text.setOnClickListener {
            changeUsername()
        }

        user_avatar_image.setOnClickListener {
            changeAvatar()
        }

        options_image.setOnClickListener { view ->
            val popupMenu = PopupMenu(this, view)

            popupMenu.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.item_change_avatar -> {
                        changeAvatar()
                    }

                    R.id.item_change_username -> {
                        changeUsername()
                    }

                    R.id.item_log_out -> {
                        logOut()
                    }
                }
                true
            }

            popupMenu.inflate(R.menu.profile_options_menu)

            try {
                showMenuIcons(popupMenu)
            } catch (e: Exception) {
                Log.e("ERROR", "Error showing menu icons", e)
            } finally {
                popupMenu.show()
            }
        }

        local_image.setOnClickListener {
            startActivityWithoutAnimation(this, UserChoosersActivity::class.java)
        }

        worldwide_image.setOnClickListener {
            startActivityWithoutAnimation(this, AllChoosersActivity::class.java)
        }

        settings_image.setOnClickListener {
            startActivityWithoutAnimation(this, SettingsActivity::class.java)
        }
    }

    private fun changeAvatar() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
            1
        )
    }

    private fun changeUsername() {
        val changeUsernameDialog = Dialog(this)
        changeUsernameDialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(true)
            setContentView(R.layout.change_username_dialog)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            username_edit_text.setText("")
            change_username_dialog_layout.setBackgroundLayout()

            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            username_edit_text.requestFocus()

            save_new_username_text.setAccentColor(context)
            username_edit_text.setText(currentUser.username)
            username_edit_text.setSelection(currentUser.username.length)

            username_edit_text.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (username_edit_text.text.toString().isNotBlank()) {
                        save_new_username_text.setAccentColor(this@ProfileActivity)
                    } else {
                        save_new_username_text.setNotActiveColor(this@ProfileActivity)
                    }
                }

                override fun afterTextChanged(s: Editable?) {}
            })

            save_new_username_text.setOnClickListener {
                val newUsername = username_edit_text.text.toString()
                if (newUsername.isNotBlank()) {
                    currentUser.username = newUsername
                    val usernameReference = getUsernameReference(currentUser.uid)
                    usernameReference.setValue(newUsername)
                    changeUsernameText(newUsername)
                    dismiss()
                } else {
                    showToast(this@ProfileActivity, getString(R.string.enter_new_username))
                }
            }

            cancel_text.setOnClickListener {
                dismiss()
            }

            setOnDismissListener {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
            }

            show()
        }
    }

    private fun changeUsernameText(newUsername: String) {
        username_text.text = newUsername
    }

    private fun logOut() {
        FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun loadTopChoosersList() {
        val userChoosersListReference = getChoosersListReference(currentUser.uid)
        userChoosersListReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val listOfChoosers = arrayListOf<Chooser>()
                snapshot.children.forEach {
                    val userChooser = it.getValue(Chooser::class.java)!!
                    listOfChoosers.add(userChooser)
                }
                listOfChoosers.sortByDescending { it.numberOfCompletions }
                when (listOfChoosers.size) {
                    0 -> no_top_choosers_text.visibility = View.VISIBLE
                    1 -> topChooserAdapter.add(
                        TopChooserItem(
                            listOfChoosers[0],
                            0,
                            this@ProfileActivity
                        )
                    )
                    2 -> {
                        for (i in 0..1) {
                            topChooserAdapter.add(
                                TopChooserItem(
                                    listOfChoosers[i],
                                    i,
                                    this@ProfileActivity
                                )
                            )
                        }
                    }
                    else -> {
                        for (i in 0..2) {
                            topChooserAdapter.add(
                                TopChooserItem(
                                    listOfChoosers[i],
                                    i,
                                    this@ProfileActivity
                                )
                            )
                        }
                    }
                }
                top_choosers_loading_circle.visibility = View.GONE
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@ProfileActivity, error.message)
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent =
                    Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                intent.type = "image/*"
                startActivityForResult(intent, 0)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && data != null) {
            user_avatar_image.setImageURI(data.data)
            val filename = UUID.randomUUID().toString()
            val bytesImage = prepareImageForSaving(data.data!!)

            FirebaseStorage.getInstance().getReference("/images/$filename").putBytes(bytesImage)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        FirebaseStorage.getInstance().reference.child("images")
                            .child(filename).downloadUrl.addOnSuccessListener { uri ->
                                val userProfileImageReference =
                                    getUserProfileImageUrlReference(currentUser.uid)
                                userProfileImageReference.setValue(uri.toString())
                                val previousProfileImageUri =
                                    currentUser.profileImageUrl.substringAfter("F")
                                        .substringBefore("?")
                                FirebaseStorage.getInstance()
                                    .getReference("/images/$previousProfileImageUri").delete()
                                currentUser.profileImageUrl = uri.toString()
                            }
                    }
                }
        }
    }

    @SuppressLint("Recycle")
    @Suppress("DEPRECATION")
    private fun prepareImageForSaving(image: Uri): ByteArray {
        var path: String? = null
        val projection = arrayOf(MediaStore.MediaColumns.DATA)
        val cr = applicationContext.contentResolver
        val metaCursor = cr.query(image, projection, null, null, null)
        metaCursor.use {
            if (it!!.moveToFirst()) {
                path = it.getString(0)
            }
        }

        val matrix = Matrix()
        var rotate = 0

        if (path != null) {
            val exif = ExifInterface(path!!)

            when (exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            }
        }

        matrix.postRotate(rotate.toFloat())
        val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, image)
        val rotatedBitmap =
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        val bios = ByteArrayOutputStream()
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bios)
        return bios.toByteArray()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, UserChoosersActivity::class.java))
        AcTrans.Builder(this).performSlideToRight()
    }
}