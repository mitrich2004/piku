package com.mitrich.piku.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.mitrich.piku.*
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.activities.UserChoosersActivity.Companion.currentUser
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.data.Element
import com.mitrich.piku.data.User
import com.mitrich.piku.utils.*
import com.mitrich.piku.views.AddElementItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_chooser.*
import kotlinx.android.synthetic.main.activity_create_chooser.*
import kotlinx.android.synthetic.main.change_username_dialog.*
import java.util.*
import kotlin.math.pow

class CreateChooserActivity : AppCompatActivity() {

    companion object {
        var allElementsSaved = true
    }

    private var elementsList = arrayListOf<Element>()
    private val elementsAdapter = GroupAdapter<ViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_chooser)

        create_chooser_toolbar.setBackgroundColor()
        add_elements_recycler_view.setBackgroundColor()
        number_of_elements_seek_bar.setAccentColor(this)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
        chooser_name_edit_text.requestFocus()

        elementsList.add(Element(getString(R.string.untitled), 0, 0, UUID.randomUUID().toString()))
        elementsList.add(Element(getString(R.string.untitled), 0, 0, UUID.randomUUID().toString()))

        add_elements_recycler_view.adapter = elementsAdapter

        for (i in 0 until elementsList.size) {
            elementsAdapter.add(
                AddElementItem(
                    elementsList[i],
                    i,
                    imm,
                    elementsAdapter,
                    elementsList,
                    save_image,
                    chooser_name_edit_text,
                    this
                )
            )
        }

        create_chooser_back_image.setOnClickListener {
            clearAdapterAndElementsList()
            allElementsSaved = true
            finish()
        }

        save_image.setOnClickListener {
            val chooserName = chooser_name_edit_text.text.toString()
            if (chooserName.isNotBlank() && chooserName.isNotEmpty()) {
                if (allElementsSaved) {
                    saveNewChooser(chooserName)
                } else {
                    showToast(this, getString(R.string.save_elements))
                }
            } else {
                showToast(this, getString(R.string.name_for_your_chooser))
            }
        }

        val seekBarChangeListener = object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                val numberOfItems = 2.toFloat().pow(seekBar.progress + 1).toInt()
                seek_bar_value_text.text = numberOfItems.toString()

                if (elementsList.size > numberOfItems) {
                    while (elementsList.size > numberOfItems) {
                        elementsList.removeAt(elementsList.size - 1)
                        elementsAdapter.removeGroup(elementsAdapter.groupCount - 1)
                    }
                } else {
                    while (elementsList.size < numberOfItems) {
                        elementsList.add(
                            Element(
                                getString(R.string.untitled),
                                0,
                                0,
                                UUID.randomUUID().toString()
                            )
                        )
                        elementsAdapter.add(
                            AddElementItem(
                                elementsList[elementsList.size - 1],
                                elementsList.size - 1,
                                imm,
                                elementsAdapter,
                                elementsList,
                                save_image,
                                chooser_name_edit_text,
                                this@CreateChooserActivity
                            )
                        )
                    }
                }
            }
        }
        number_of_elements_seek_bar.setOnSeekBarChangeListener(seekBarChangeListener)

        chooser_name_edit_text.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    verifySaveImageState()
                }
            })

    }

    private fun verifySaveImageState() {
        if (chooser_name_edit_text.text.toString().isNotBlank() && allElementsSaved) {
            save_image.setAccentColor(this)
        } else {
            save_image.setNotActiveColor(this)
        }
    }

    private fun saveNewChooser(chooserName: String) {
        val newChooser = Chooser(
            UUID.randomUUID().toString(),
            chooserName, elementsList, currentUser.uid, 0, 0
        )
        val userReference = getUserReference(currentUser.uid)
        userReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(User::class.java)!!
                currentUser = user
                currentUser.choosersList.add(newChooser)
                val choosersListReference = getChoosersListReference(currentUser.uid)
                choosersListReference.setValue(currentUser.choosersList)
                clearAdapterAndElementsList()
                val userChoosersIntent =
                    Intent(this@CreateChooserActivity, UserChoosersActivity::class.java)
                startActivity(userChoosersIntent)
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@CreateChooserActivity, error.message)
            }
        })
    }

    private fun clearAdapterAndElementsList() {
        elementsAdapter.clear()
        elementsList.clear()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        clearAdapterAndElementsList()
    }
}