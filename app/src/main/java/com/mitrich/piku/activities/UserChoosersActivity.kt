package com.mitrich.piku.activities

import android.content.Intent
import android.content.res.Configuration
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.mitrich.piku.R
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.data.User
import com.mitrich.piku.data.UserSettings
import com.mitrich.piku.utils.*
import com.mitrich.piku.views.ChooserItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_user_choosers.*
import java.io.File
import java.util.*
import kotlin.system.exitProcess

class UserChoosersActivity : AppCompatActivity() {

    companion object {
        var currentUser = User()
        var appThemeId = R.style.AppThemeLight
        var accentColorId = R.color.accent_orange
        var languageTag = "ru"
    }

    private val choosersAdapter = GroupAdapter<ViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {

        if (File("$filesDirection/$userSettingsFileName").exists()) {
            File("$filesDirection/$userSettingsFileName").forEachLine {
                val userSettings = Gson().fromJson(it, UserSettings::class.java)
                appThemeId = userSettings.appThemeId
                languageTag = userSettings.languageTag
                accentColorId = userSettings.accentColorId
            }
        } else {

            File.createTempFile("userSettings", ".json")

            languageTag = if (Locale.getDefault().displayLanguage == "русский") {
                "ru"
            } else {
                "en"
            }

            val deviceDarkTheme = resources.configuration.uiMode and
                    Configuration.UI_MODE_NIGHT_MASK == UI_MODE_NIGHT_YES

            appThemeId =
                if (deviceDarkTheme) {
                    R.style.AppThemeDark
                } else {
                    R.style.AppThemeLight
                }

            File("$filesDir/$userSettingsFileName").writeText(
                Gson().toJson(
                    UserSettings(
                        languageTag,
                        appThemeId,
                        accentColorId
                    )
                )
            )
        }

        setLocale(languageTag, baseContext)
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_choosers)

        if (!isInternetAvailable(this)) {
            user_choosers_loading_circle.visibility = View.GONE
            user_choosers_bottom_toolbar.visibility = View.GONE
            create_new_chooser_button.visibility = View.GONE
            no_internet_image.visibility = View.VISIBLE
            no_internet_text.visibility = View.VISIBLE
        }

        verifyUserLoggedIn()
        user_choosers_recycler_view.adapter = choosersAdapter
        local_image.setAccentColor(this)
        user_choosers_loading_circle.setAccentColor(this)
        user_choosers_bottom_toolbar.setBackgroundColor()
        create_new_chooser_button.setAccentColor()

        choosersAdapter.setOnItemClickListener { item, _ ->
            val chooserItem = item as ChooserItem
            intentToChooserActivity(this, chooserItem, true)
        }

        choosersAdapter.setOnItemLongClickListener { item, view ->
            val selectedListItem = item as ChooserItem
            val selectedChooser = selectedListItem.chooser
            val popupMenu = PopupMenu(this, view)

            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.item_edit_chooser -> {
                        editChooser(selectedChooser)
                    }

                    R.id.item_delete_chooser -> {
                        choosersAdapter.remove(item)
                        deleteChooser(selectedChooser)
                    }

                    else -> {
                        //skip
                    }
                }
                true
            }

            popupMenu.inflate(R.menu.chooser_menu)

            try {
                showMenuIcons(popupMenu)
            } catch (e: Exception) {
                Log.e("ERROR", "Error showing menu icons", e)
            } finally {
                popupMenu.show()
            }

            true
        }

        create_new_chooser_button.setOnClickListener {
            startActivity(Intent(this, CreateChooserActivity::class.java))
        }

        worldwide_image.setOnClickListener {
            startActivityWithoutAnimation(this, AllChoosersActivity::class.java)
        }

        profile_image.setOnClickListener {
            startActivityWithoutAnimation(this, ProfileActivity::class.java)
        }

        settings_image.setOnClickListener {
            startActivityWithoutAnimation(this, SettingsActivity::class.java)
        }
    }

    private fun verifyUserLoggedIn(): Boolean {
        val uid = FirebaseAuth.getInstance().uid
        return if (uid == null) {
            val registerActivityIntent = Intent(this, RegisterActivity::class.java)
            registerActivityIntent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(registerActivityIntent)
            false
        } else {
            fetchCurrentUser()
            true
        }
    }

    private fun fetchCurrentUser() {
        val uid = FirebaseAuth.getInstance().uid!!
        val userReference = getUserReference(uid)
        userReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                currentUser = snapshot.getValue(
                    User::class.java
                )!!
                fetchUserChoosersList(currentUser.uid)
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@UserChoosersActivity, error.message)
            }
        })
    }

    private fun fetchUserChoosersList(userId: String) {
        val chooserListReference = getChoosersListReference(userId)
        chooserListReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val chooser = it.getValue(Chooser::class.java)!!
                    choosersAdapter.add(ChooserItem(chooser, this@UserChoosersActivity))
                }
                user_choosers_loading_circle.visibility = View.GONE
                if (choosersAdapter.itemCount == 0) {
                    no_choosers_text.visibility = View.VISIBLE
                }
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@UserChoosersActivity, error.message)
            }

        })
    }

    private fun editChooser(chooser: Chooser) {
        val intent = Intent(this, EditChooserActivity::class.java)
        intent.putExtra("chooser", chooser)
        for (userChooser in currentUser.choosersList) {
            if (userChooser.id == chooser.id) {
                intent.putExtra(
                    "chooserIndex",
                    currentUser.choosersList.indexOf(userChooser)
                )
            }
        }
        startActivity(intent)
    }

    private fun deleteChooser(chooser: Chooser) {
        currentUser.choosersList.remove(chooser)
        val choosersListReference = getChoosersListReference(currentUser.uid)
        choosersListReference.setValue(currentUser.choosersList)

        usersReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach { dataSnapshot ->
                    val user = dataSnapshot.getValue(User::class.java)!!

                    val likedChoosersListReference = getLikedChoosersListReference(user.uid)

                    for (chooserId in user.likedChoosersList) {
                        if (chooserId == chooser.id) {
                            user.likedChoosersList.remove(
                                chooserId
                            )
                            likedChoosersListReference.setValue(
                                user.likedChoosersList
                            )
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@UserChoosersActivity, error.message)
            }
        })

        if (choosersAdapter.itemCount == 0) {
            no_choosers_text.visibility = View.VISIBLE
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }

    override fun onRestart() {
        super.onRestart()
        choosersAdapter.clear()
        fetchUserChoosersList(currentUser.uid)
    }
}
