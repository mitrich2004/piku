package com.mitrich.piku.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.mitrich.piku.views.ChooserItem
import com.mitrich.piku.R
import com.mitrich.piku.activities.UserChoosersActivity.Companion.appThemeId
import com.mitrich.piku.data.Chooser
import com.mitrich.piku.data.User
import com.mitrich.piku.utils.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_choosers.*

class ChoosersActivity : AppCompatActivity() {
    val choosersAdapter = GroupAdapter<ViewHolder>()
    var user = User()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeId)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choosers)

        user = intent.getParcelableExtra(INTENT_KEY_USER)!!

        choosers_recycler_view.adapter = choosersAdapter

        choosers_toolbar_title.text = user.username
        choosers_toolbar.setBackgroundColor()
        user_avatar_image.loadUserAvatar(user.profileImageUrl)
        user.choosersList.sortByDescending { it.numberOfCompletions }

        fetchUserChoosersList(user.uid)

        choosersAdapter.setOnItemClickListener { item, _ ->
            val chooserItem = item as ChooserItem
            intentToChooserActivity(this, chooserItem, false)
        }

        choosers_back_image.setOnClickListener {
            finish()
        }
    }

    private fun fetchUserChoosersList(userId: String) {
        val chooserListReference = getChoosersListReference(userId)
        chooserListReference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    val chooser = it.getValue(Chooser::class.java)!!
                    choosersAdapter.add(ChooserItem(chooser, this@ChoosersActivity))
                }
            }

            override fun onCancelled(error: DatabaseError) {
                showToast(this@ChoosersActivity, "Failed to load choosers")
            }

        })
    }

    override fun onRestart() {
        super.onRestart()
        choosersAdapter.clear()
        fetchUserChoosersList(user.uid)
    }

}